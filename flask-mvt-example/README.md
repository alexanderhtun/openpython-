#python virtual envrionment 
pip install virtualenv

python3 -m venv env

#activate the virtual environment 
source env/bin/activate

#mysql database creation 
mysql -u root -p 

create user 'py_admin'@'localhost' indentified by 'py2020';

create database pytest_db;

grant all privileges on pytest_db . * to 'py_admin'@'localhost';

#we need to create the migrations directory based on the db models 
#migration 
flask db init

flask db migrate

flask db upgrade

#run the flask application [you must be in the env] 
export FLASK_CONFIG=development

export FLASK_APP=run.py

flask run

#unit testing [you must be in the env] 
#for the database setup 
mysql -u root -p 

create database testing_db; 

GRANT ALL PRIVILEGES ON testing_db . * TO 'py_admin'@'localhost';

#testing script [CI purpose] 
python tests.py 
