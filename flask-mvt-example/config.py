class Config: 
    """
    commom configuration  
    """ 
    DEBUG = True 
    # nothing 



class DevelopmentConfig(Config): 
    """
    Development Configuration  
    """ 

    #DEBUG = True 
    SQLALCHEMY_ECHO = True 

class ProductionConfig(Config): 
    """
    Production Configuration  
    """ 
    DEBUG = False 

class TestingConfig(Config):
    """
    Testing configurations
    """

    TESTING = True

app_config = {
    'development' : DevelopmentConfig, 
    'production'  : ProductionConfig, 
    'testing' : TestingConfig
}
