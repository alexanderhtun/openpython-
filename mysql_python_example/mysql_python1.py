#Mysql နှင့်စတင်ချိတ်ဆက်လုပ်ဆောင်ခြင်း 

import mysql.connector

# mysql နှင့်စတင်၍ချိတ်ဆက်အသုံးပြုမည်ဖြစ်သည်။ သို့ဖြစ်၍ DB၏အမည်နှင့် သက်ဆိုင်ရာ အချက်အလက်များကို
#
# ဖြည့်စွက်ပေးရန်လိုအပ်သည် 

db = mysql.connector.connect(host="localhost",
                                         database='dummy_DB',
                                         user="dummy_user",
                                         password="P@55worRD123")
                                         
                                         
cur = db.cursor()

cur.execute("SELECT * FROM examples")

for row in cur.fetchall():
    print(row[0]," ",row[1])
    
# table များကိုပြသပေးရန်ဖြစ်သည် 

cur.execute("SHOW TABLES")

#Table ထဲသို့ထည့်သွင်းပေးရန်

sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = ("John", "Highway 21")
cur.execute(sql, val)