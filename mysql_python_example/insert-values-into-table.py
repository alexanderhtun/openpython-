import mysql.connect 

db = mysql.connector.connect(host="localhost",
                                         database='python_DB',
                                         user="dummy_db",
                                         password="P@55w0rd123")

cursor = db.cursor()

sql_command = "INSERT INTO customers (name, address, telephone_number,email_address) VALUES (%s, %s, %s, %s)"
data_to_insert = [
    ("Bob", "Amayar Street", "234-212310", "Bob@amayar.com"), 
    ("Alice", "Mahar Stree", "221-235321", "Alice@mahar.com")
]

cursor.executemany(sql_command, data_to_insert) 
cursor.commit() 