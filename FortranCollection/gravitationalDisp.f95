program gravitationalDis
    implicit none

    !gravitation acceleration 
    real, parameter :: g = 9.81

    !variable declaration 
    real :: s 
    real :: t 
    real :: u 

    !values 
    t = 5.0 
    u = 20 

    !displacement 
    s = u*t - g*(t**2)/2 

    !output 
    print *,"Time =", t 
    print *, "Displacement =", s

end program gravitationalDis